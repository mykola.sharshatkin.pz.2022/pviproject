function addRowToTable() {

    if (firstName.value == "" || lastName.value == "" || birthday.value == "") {
        return;
    }

    let table = document.getElementById("main-table");
    let confirmButtonInfo = document.getElementById("confirm-btn-info");
    let index = confirmButtonInfo.value;
    if (index != -1) {
        deleteRowIndex(index);
    }
    let row = table.insertRow(index);
    let cell = row.insertCell(0);
    cell.innerHTML = "<td><input type = checkbox class=checkbox-btn></td>";
    cell = row.insertCell(1);
    cell.innerHTML = group.value;
    cell = row.insertCell(2);
    cell.innerHTML = firstName.value + " " + lastName.value;
    cell = row.insertCell(3);
    cell.innerHTML = gender.value;
    cell = row.insertCell(4);
    cell.innerHTML = birthday.value;
    cell = row.insertCell(5);
    cell.innerHTML = "<td><input type = radio class=status-btn></td>";
    cell = row.insertCell(6);
    cell.innerHTML =    "<td>\
                            <button class=options-btn onclick=showInfoDialog(this.parentNode.parentNode.rowIndex)>\
                                <img src=edit-icon.png alt='Edit Student' class=button-image>\
                            </button>\
                            <button class=options-btn onclick=showDeleteDialog(this.parentNode.parentNode.rowIndex)>\
                                <img src=delete-icon.png alt='Delete Student' class=button-image>\
                            </button>\
                        </td>";
}

function showDeleteDialog(index) {
    let closeDialog = document.getElementById("close-button-dialog");
    let confirmButton = document.getElementById("confirm-btn");
    confirmButton.value = index;

    let table = document.getElementById("main-table");
    let name = table.rows[index].cells[2].textContent;
    closeDialog.getElementsByTagName("p")[0].textContent = "Are you sure you want to delete user " + name + " ?";
    closeDialog.showModal();
}

function deleteRow() {
    let confirmButton = document.getElementById("confirm-btn");
    let index = confirmButton.value;
    let table = document.getElementById("main-table");
    table.rows[index].remove();
}

function deleteRowIndex(index) {
    let table = document.getElementById("main-table");
    table.rows[index].remove();
}

function showInfoDialog(index) {
    let infoDialog = document.getElementById("info-dialog");
    let confirmButtonInfo = document.getElementById("confirm-btn-info");
    confirmButtonInfo.value = index;

    infoDialog.getElementsByTagName("form")[0].reset();
    if (index != -1) {
        let table = document.getElementById("main-table");
        let row = table.rows[index];
        group.value = row.cells[1].textContent;
        let name = row.cells[2].textContent.split(" ");
        firstName.value = name[0];
        lastName.value = name[1];
        gender.value = row.cells[3].textContent;
        birthday.value = row.cells[4].textContent;
        infoDialog.getElementsByTagName("form")[0].getElementsByTagName("p")[0].textContent = "Edit Student";
    } else {
        infoDialog.getElementsByTagName("form")[0].getElementsByTagName("p")[0].textContent = "Add Student";
    }
    infoDialog.showModal();
}

function closeDialog(dialog) {
    dialog.close();
}

function tableCheckboxClicked() {
    let checkbox = document.getElementById("table-head-checkbox");
    let table = document.getElementById("main-table");
    for (let i = 1; i < table.rows.length; i++) {
        let rowCheckBox = table.rows[i].cells[0].firstChild;
        rowCheckBox.checked = checkbox.checked;
    }
}
